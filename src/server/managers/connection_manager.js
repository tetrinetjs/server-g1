//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');

/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */
const initConnectionManager = function(app, clientRepository){

	
	app.setOnConnectionListener((socket) => {
		if(!clientRepository.hasEmptySlot()){
			const reason = 'Server is full';
			const noConnectingMessage = `noconnecting ${reason}`;
			socket.writeTetrinetMessage(noConnectingMessage);
			return false;
		}

		return true;
	});

	app.onReceive('tetrisstart', (params, socketSender) => {
		params = String(params);
		console.log('echo: '+params);
		const clientVersion = params.split(' ')[1];
		const playerName = params.split(' ')[0];
		if(clientVersion === '1.13'){

			const newClient = clientRepository.addClient(playerName, socketSender);
			let winlist = "winlist:\ntTourTheAbyss ; 922\npFregge ; 855\npelissa ; 475";
			newClient.socket.writeTetrinetMessage(winlist);
	
			let loginResponse = "\nplayernum: "+newClient.id+"\0";
			newClient.socket.writeTetrinetMessage(loginResponse);
	
			//notify other players about connection
			const connectionMessage = "\nplayerjoin: "+newClient.id+" ; "+newClient.name+"\0";
			clientRepository.broadcastAll(connectionMessage);

			//notify new client about others that are in the server
			clientRepository.connectedClients.forEach((client) => {
				const connectionMessage = "\nplayerjoin: "+client.id+" ; "+client.name+"\0";
				newClient.socket.writeTetrinetMessage(connectionMessage);
			})
		}
	});

	app.onReceive('team', (params, socketSender) => {

	})

	app.setOnSocketDisconnectCallback((socket) => {
		let disconnectedClient = clientRepository.removeClientBySocket(socket);
		const message = 'playerleave;'+disconnectedClient;
		clientRepository.broadcastAll(message);
	});

	
}

module.exports = initConnectionManager;